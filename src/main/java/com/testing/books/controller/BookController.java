package com.testing.books.controller;

import com.testing.books.dto.BookDTO;
import com.testing.books.service.BookService;
import com.testing.books.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookService bookService;

    // Get count of all books
    @GetMapping("/count")
    public ResponseEntity<BigDecimal> getCountOfAllBooks() {
        return new ResponseEntity<>(bookService.getCountOfAllBooks(), HttpStatus.OK);
    }

    // Get all books
    @GetMapping
    public ResponseEntity<List<Book>> getAllBookRecords() {
        List<Book> books = bookService.getAllBookRecords();
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    // Get a book by ID
    @GetMapping("/{bookId}")
    public ResponseEntity<Book> getBookById(@PathVariable("bookId") Long bookId) {
        Book book = bookService.getBookById(bookId);
        return new ResponseEntity<>(book, HttpStatus.OK);
    }

    // Create a new book
    @PostMapping("/create-book")
    public ResponseEntity<Book> createBookRecord(@RequestBody Book book) {
        Book savedBook = bookService.createBookRecord(book);
        return new ResponseEntity<>(savedBook, HttpStatus.CREATED);
    }

    // Update a book
    @PutMapping("/update-book/{bookId}")
    public ResponseEntity<Book> updateBook(@PathVariable("bookId") Long bookId, @RequestBody BookDTO bookDTO) {
        Book updatedBook = bookService.updateBook(bookId, bookDTO);
        return new ResponseEntity<>(updatedBook, HttpStatus.OK);
    }

    // Delete a book
    @DeleteMapping("/delete-book/{bookId}")
    public ResponseEntity<Void> deleteBook(@PathVariable("bookId") Long bookId) {
        bookService.deleteBook(bookId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
